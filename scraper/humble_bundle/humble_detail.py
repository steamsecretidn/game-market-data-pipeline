import requests
import json
import pandas as pd
import random

'''
This is base URL is
https://www.humblebundle.com/store/api/lookup?products%5B%5D=[human_url]&request=1&edit_mode=false
U can change the [human_url] parameters with humblebundle human_url
'''

status = 'DONE'

humble_data = pd.read_csv('../../data/humble_data.json')

warehouse_append = open(
	'/home/zef/Documents/game-market-data-pipeline/data/humble_details.json',
	'ab'
)

list_of_files = []

for human_url in humble_data['human_url']:
    response_html = requests.get('https://www.humblebundle.com/store/api/lookup?products%5B%5D='
        + str(human_url)
        + '&request=1&edit_mode=false').json()

    if len(response_html['results']) != 0:
        for data in response_html['results']:
            new_data = {
                'stripped_description': data['stripped_description'],
                'human_url': human_url,
                'stok': random.randint(1, 5)
            }

            carousel_content = data['carousel_content']
            screenshot = carousel_content['screenshot']

            start = 2

            for i in screenshot:
                column = "gambar_%d"%start
                new_data[column] = i['compressed_image']

            list_of_files.append(new_data)

json.dump(list_of_files, warehouse_append)
print status
