import requests
import json
import time

'''
This is base from https://www.humblebundle.com/store/api?request=1&page_size=20&sort=bestselling&page=0
U can change the page numbers
'''

""" OPEN WITH READ AND WRITE PERMISSION, CREATE IF NECESSARY, THE FILE """
warehouse_append = open(
	'/home/zef/Documents/game-market-data-pipeline/data/humble_data.json',
	'ab'
)

list_of_files = []

max_page = 259 + 1
status = "DONE"
for page in range(1, max_page):
	if page % 10 == 0:
		print "Processing page %d..."%page
	response_html = requests.get('https://www.humblebundle.com/store/api?request=1&page_size=20&sort=bestselling&page=%d'%page).json()
	time.sleep(1)
	if len(response_html['results']) != 0:
		for data in response_html['results']:
			new_data = {}
			if 'storefront_featured_image_small' in data:
				new_data['storefront_featured_image_small'] = data['storefront_featured_image_small']
			else:
				new_data['storefront_featured_image_small'] = 'none'

			if 'storefront_v2_large_capsule' in data:
				new_data['storefront_v2_large_capsule'] = data['storefront_v2_large_capsule']
			else:
				new_data['storefront_v2_large_capsule'] = 'none'

			if 'blocked_territories' in data:
				new_data['blocked_territories'] = data['blocked_territories']
			else:
				new_data['blocked_territories'] = 'none'

			if 'delivery_methods' in data:
				new_data['delivery_methods'] = data['delivery_methods']
			else:
				new_data['delivery_methods'] = 'none'

			if 'human_url' in data:
				new_data['human_url'] = data['human_url']
			else:
				new_data['human_url'] = 'none'

			if 'content_types' in data:
				new_data['content_types'] = data['content_types']
			else:
				new_data['content_types'] = 'none'

			if 'current_price' in data:
				new_data['current_price'] = data['current_price'][0]
			else:
				new_data['current_price'] = 'none'

			if 'cta_badge' in data and data['cta_badge'] is not None:
				new_data['cta_badge'] = data['cta_badge']
			else:
				new_data['cta_badge'] = 'none'

			if 'full_price' in data:
				new_data['full_price'] = data['full_price'][0]
			else:
				new_data['full_price'] = 'none'

			if 'human_name' in data:
				new_data['human_name'] = data['human_name']
			else:
				new_data['full_price'] = 'none'


			if 'full_price' in data and data['full_price'] > data['current_price']:
				new_data['isDiscount'] = "true"
			else:
				new_data['isDiscount'] = 'false'

			if 'current_price' in data:
				new_data['currency'] = data['current_price'][1]
			elif 'full_price' in data:
				new_data['currency'] = data['full_price'][1]
			else:
				new_data['currency'] = 'USD'

			data = new_data

			if new_data['isDiscount'] == 'true' and 'ID' not in new_data['blocked_territories']:
				list_of_files.append(data)

json.dump(list_of_files, warehouse_append)
print status
