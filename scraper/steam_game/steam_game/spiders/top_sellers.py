import scrapy
from scrapy.spiders import BaseSpider
from scrapy.selector import Selector
from scrapy.http import Request
import time

cookies = {
    'birthtime':'631126801',
    'browserid':'1082397459534260447',
    'lastagecheck':'1-January-1990',
    'maturecontent':'1',
    'steamCountry':'ID%7Cfa692a970eb2b1e6c584f3bb206deaee'
}

class TopSellers(BaseSpider):

    name = "top_sellers"
    allowed_domains = ["http://store.steampowered.com"]
    start_urls = [
        "http://store.steampowered.com/search/?category1=998&filter=topsellers&page=1"
    ]

    def __init__(self):
        self.base_url = "http://store.steampowered.com/search/?category1=998&filter=topsellers&page="
        self.page = 2

    def start_requests(self):
        for i in range(self.page, 501):
            url = self.base_url + str(i)
            self.page = self.page + 1
            yield Request(url=url, cookies=cookies, callback=self.parse)


    def parse(self, response):
        xpath_selector = Selector(response)
        games = xpath_selector.xpath(
            "//*[@id='search_result_container']/div/a"
        )

        for game in games:
            # Game Identity
            title = game.xpath(".//span[@class='title']/text()").extract()[0]

            link = (
                game
                .xpath(
                    "./@href"
                )
                .extract()[0]
                .split('/')
            )

            def repair_game_link(data):
                holder = 0
                for i in xrange(len(data)):
                    if data[i].isdigit():
                        gameId = data[i]
                        holder = i
                        break
                    else:
                        continue
                data = '/'.join(data[:i+1])
                return data, gameId

            link,gameId = repair_game_link(link)

            releaseDate = (
                game
                .xpath(".//div[@class='col search_released responsive_secondrow']/text()")
                .extract()
            )

            if len(releaseDate) > 0:
                releaseDate = releaseDate[0]
                releaseDate = releaseDate.replace(',', ' ')
            else:
                releaseDate = None

            # Community Review
            try:
                communityReview = (
                    game
                    .xpath(".//span[@class='search_review_summary positive']/@data-store-tooltip")
                    .extract()
                )
            except:
                try:
                    communityReview = (
                        game
                            .xpath(".//span[@class='search_review_summary mixed']/@data-store-tooltip")
                            .extract()
                    )
                except:
                    try:
                        communityReview = (
                            game
                                .xpath(".//span[@class='search_review_summary negative']/@data-store-tooltip")
                                .extract()
                        )
                    except:
                        pass

            if len(communityReview) < 1:
                communityReview = "No Rating Yet"
            else:
                communityReview = str(communityReview[0]).replace('<br>', ' ')

            # Pricing
            price_div = (
                game
                .xpath(
                    ".//div[@class='col search_price_discount_combined responsive_secondrow']"
                )
            )

            totalDiscount = (
                price_div
                .xpath(
                    ".//div[@class='col search_discount responsive_secondrow']/span/text()"
                )
                .extract()
            )

            if '%' in str(totalDiscount):
                totalDiscount = totalDiscount[0]
                isDiscount = 'yes'
            else:
                isDiscount = 'no'
                totalDiscount = '0%'

            if isDiscount == 'yes':
                regularPrice = str(
                    price_div
                    .xpath(
                    ".//div[@class='col search_price discounted responsive_secondrow']//strike/text()"
                    )
                    .extract()[0]
                )

                discountPrice = (
                    str(
                        price_div
                        .xpath(
                            ".//div[@class='col search_price discounted responsive_secondrow']/text()"
                        )
                        .extract()[1]
                    )
                    .encode('string_escape')
                    .replace('\\n', '')
                    .replace('\\t', '')
                    .replace('\\r', '')
                )
            elif isDiscount == 'no':
                discountPrice = 'none'

                regularPrice = (
                    str(
                        price_div
                        .xpath(
                            ".//div[@class='col search_price  responsive_secondrow']/text()"
                        )
                        .extract()[0]
                    )
                    .encode('string_escape')
                    .replace('\\n', '')
                    .replace('\\t', '')
                    .replace('\\r', '')
                )

            data = {}

            data['title'] = title
            data['gameId'] = gameId
            data['link'] = link
            data['releaseDate'] = releaseDate
            data['communityReview'] = communityReview
            data['regularPrice'] = regularPrice
            data['isDiscount'] = isDiscount
            data['totalDiscount'] = totalDiscount
            data['discountPrice'] = discountPrice
            data['modifiedAt'] = int(time.time())

            yield data