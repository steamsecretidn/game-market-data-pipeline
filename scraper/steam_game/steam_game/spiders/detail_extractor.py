import scrapy
from scrapy.spiders import BaseSpider
from scrapy.selector import Selector
from scrapy.http import Request
import pandas as pd

cookies = {
    'birthtime':'631126801',
    'browserid':'1082397459534260447',
    'lastagecheck':'1-January-1990',
    'maturecontent':'1',
    'steamCountry':'ID%7Cfa692a970eb2b1e6c584f3bb206deaee'
}

class TopSellers(BaseSpider):

    name = "detail_extractor"
    allowed_domains = ["http://store.steampowered.com"]
    start_urls = [
        "http://store.steampowered.com/search/?category1=998&filter=topsellers&page=1"
    ]

    def __init__(self):
        self.base_url = "http://store.steampowered.com/search/?category1=998&filter=topsellers&page="
        self.page = 2

    def start_requests(self):
        game_data = pd.read_csv('../steam_game/raw_game_data.csv')
        for i in game_data['link']:
            url = i
            yield Request(url=url, cookies=cookies, callback=self.parse)

    def parse(self, response):
        xpath_selector = Selector(response)

        gameId = str(response.url).split('/')

        for i in gameId:
            if i.isdigit():
                gameId = i
                break

        genre = (
            xpath_selector
            .xpath(
                "//*[@class='blockbg']/a[2]/text()"
            ).extract()
        )

        if len(genre) > 0:
            genre = genre[0]
        else:
            genre = None

        descriptionSnippet = ''
        description = ''
        image = ''

        if genre is None:
            pass
        else:
            descriptionSnippet = (
                xpath_selector
                    .xpath(
                    "//*[@class='game_description_snippet']/text()"
                ).extract()
            )

            if len(descriptionSnippet) > 0:
                descriptionSnippet = descriptionSnippet[0]

            descriptionSnippet = (
                descriptionSnippet
                .replace('\n', '')
                .replace('\t', '')
                .replace('\r', '')
            )

            description = (
                xpath_selector
                    .xpath(
                    "//*[@class='game_area_description' and @id='game_area_description']/text()"
                ).extract()
            )

            description = (
                ''.join(description)
                .replace('\n', '')
                .replace('\t', '')
                .replace('\r', '')
            )

            image = (
                xpath_selector
                    .xpath(
                    "//*[@class='game_header_image_full']/@src"
                ).extract_first()
            )

            data = {}

            data['gameId'] = gameId
            data['genre'] = genre
            data['descriptionSnippet'] = descriptionSnippet
            data['description'] = description
            data['image'] = image

            yield data
