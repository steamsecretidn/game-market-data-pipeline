# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class SteamGameItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    gameTitle = Field()
    releaseDate = Field()
    communityReview = Field()
    regularPrice = Field()
    isDiscount = Field()
    totalDiscount = Field()
    discountPrice = Field()
    gameId = Field()
    description = Field()
    descriptionSnippet = Field()
    genre = Field()
    gameImage = Field()
    pass
